﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    public class AvaliacaoCombustivelMD
    {
        public int Id { get; set; }
        public TipoCombustivel NomeCombustivel { get; set; }
        public double ValorCombustivel { get; set; }
        public string NomePosto { get; set; }
        public string Endereco { get; set; }
        public double Latitude { get; set; }//fornecido pelo GPS
        public double Longitude { get; set; }//fornecido pelo GPS
        public double PrecoCombust_Pontuacao { get; set; }
        public double SaldoVolumeComb_Pontuacao { get; set; }//(volume que vai abastecer - volume que vai gastar pra chegar até posto)
        public double Distancia_Pontucacao { get; set; }//distância entre o posto e o usuário

        public string SimboloCombustivel
        {
            get
            {
                switch (NomeCombustivel)
                {
                    case TipoCombustivel.Etanol: return "E";
                    case TipoCombustivel.Gasolina: return "G";
                    case TipoCombustivel.Diesel: return "D";
                    default: return "";
                }
            }
        }


        //nome alternativo:ValorRealQueVaiGastar (dinheiro que pretende abastecer + dinheiro que gasta até posto)
        //Valor total em reais de qaunto vou gastar até o posto + o valor que vou gastar no posto para encher o tanque e rodar 300 kms
        //public double ValorTotal_Pontuacao { get; set; }
        //public double CoeficienteRendimentoCombustivel { get; set; }
    }
}
