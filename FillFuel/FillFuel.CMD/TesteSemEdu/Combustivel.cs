﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    public class Combustivel
    {
        #region Construtores

        public Combustivel()
        {
        }

        public Combustivel(string tipo, float preco)
        {
            Tipo = tipo;
            Preco = preco;
        }

        #endregion

        #region Propriedades

        public string Tipo { get; set; }
        public float Preco { get; set; }
        public DateTime UltimaAtualizacao { get; set; }

        #endregion
    }
}
