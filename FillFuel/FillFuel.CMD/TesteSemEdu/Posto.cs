﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    public class Posto
    {
        #region Construtores

        public Posto()
        {
        }

        public Posto(string nome, float distanciaAtePosto, float precoEtanol, float precoGasolina, float precoDiesel)
        {
            this.Nome = nome;
            this.DistanciaAtePosto = distanciaAtePosto;//gambiarra

            Combustiveis = new List<Combustivel>
            {
                new Combustivel("etanol", precoEtanol),
                new Combustivel("gasolina", precoGasolina),
                new Combustivel("diesel", precoDiesel)
            };
        }

        #endregion

        #region Propriedades

        public string Nome { get; set; }
        public List<Combustivel> Combustiveis { get; set; }
        public float DistanciaAtePosto { get; set; }//gambiarra (para teste)

        #endregion

        #region Métodos

        /// <summary>
        /// Volume de combustível que pode ser abastecido com o valor que pretende gastar, descontado do volume de combustível necessário para ir até o posto
        /// </summary>
        /// <param name="valorQueVaiAbastecer"></param>
        /// <param name="tipoCombustivel"></param>
        /// <param name="consumoMedio"></param>
        /// <returns></returns>
        public float GetDeltaCombustivel(float valorQueVaiAbastecer, string tipoCombustivel, float consumoMedio)
        {
            float deltaCombustivel = (GetVolumeQueVaiAbastecer(valorQueVaiAbastecer, tipoCombustivel)) - (GetCombustivelGastoAtePosto(consumoMedio));
            return deltaCombustivel;
        }

        public float GetVolumeQueVaiAbastecer(float valorQueVaiAbastecer, string tipoCombustivel)
        {
            float volumeQueVaiAbastecer = valorQueVaiAbastecer / Combustiveis.Find(c => c.Tipo.Equals(tipoCombustivel)).Preco;
            return volumeQueVaiAbastecer;
        }

        public float GetCombustivelGastoAtePosto(float consumoMedio)
        {
            float combustivelGastoAtePosto = this.DistanciaAtePosto / consumoMedio;
            return combustivelGastoAtePosto;
        }

        public float GetCustoDaViagemAtePosto(float consumoMedio, string tipoCombustivel)
        {
            float custoDaViagemAtePosto = GetCombustivelGastoAtePosto(consumoMedio) * Combustiveis.Find(c => c.Tipo.Equals(tipoCombustivel)).Preco;
            return custoDaViagemAtePosto;
        }


        #endregion

        #region Métodos de listagem com filtro

        /// <summary>
        /// Retorna uma lista com postos mais pertos em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<Posto> GetPostosPertos(List<Posto> postos)
        {
            return postos.OrderBy(c => c.DistanciaAtePosto).ToList();
        }

        /// <summary>
        /// Retorna uma lista com postos mais baratos em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<Posto> GetPostosBaratos(List<Posto> postos, string tipoCombustivel)
        {
            return postos.OrderBy(posto => posto.Combustiveis.Find(combustivel => combustivel.Tipo.Equals(tipoCombustivel)).Preco).ToList();
        }

        /// <summary>
        /// Retorna uma lista com postos mais viáveis em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<Posto> GetPostosViaveis(List<Posto> postos, float valorQueVaiAbastecer, float consumoMedio, string tipoCombustivel)
        {
            return postos.OrderByDescending(c => c.GetDeltaCombustivel(valorQueVaiAbastecer, tipoCombustivel, consumoMedio)).ToList();
        }

        #endregion
    }
}
