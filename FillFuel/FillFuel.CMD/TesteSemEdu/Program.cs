﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    class Program
    {
        /// <summary>
        /// Alimenta dados fictícios para testes
        /// </summary>
        /// <returns></returns>
        static List<Posto> GetPostos()
        {
            return new List<Posto>
            {
                //essa distância é dada usando as coordenadas geográficas do posto e do usuário, fornecidas pelo Xamarin.Essencials
                new Posto("Monte Carlo", 6.5f, 2.13f, 4.15f, 3.34f),
                new Posto("Am/Pm", 20.5f, 2.63f, 3.50f, 3.35f),
                new Posto("Brazilian", 6.9f, 1.99f, 4.11f, 3.69f),
            };
        }

        static void Main(string[] args)
        {
            #region TELA INICIAL - LOGO FILLFUEL

            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("                            FILL FUEL\n");
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("                                                             by pyr\n");
            Console.WriteLine("...tecle para continuar...");
            Console.ReadKey();

            #endregion

            #region TELA DE APRESENTAÇÃO DO APP

            Console.Clear();
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("   FILL FUEL é uma aplicação desenvolvida pela equipe  PYR, capaz de \n" +
                              " calcular qual melhor custo benefício ao abastecer, levando em  con- \n" +
                              " sideração o combustível mais barato da cidade, e o posto mais perto.\n");
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("...tecle para continuar...");
            Console.ReadKey();

            #endregion

            #region TELA ESCOLHA DO TIPO DO COMBUSTÍVEL

            string tipoCombustivel = "";
            do
            {
                switch (tipoCombustivel)
                {
                    case "1":
                        tipoCombustivel = "etanol";
                        break;
                    case "2":
                        tipoCombustivel = "gasolina";
                        break;
                    case "3":
                        tipoCombustivel = "diesel";
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("---------------------------------------------------------------------\n");
                        Console.WriteLine("Escolha o combustível com o número correspondente:\n");
                        Console.WriteLine("(1 -> etanol) \n(2 -> gasolina) \n(3 -> diesel) \n");
                        Console.WriteLine("---------------------------------------------------------------------");
                        tipoCombustivel = Console.ReadLine();
                        break;
                }
            } while (tipoCombustivel != "etanol" && tipoCombustivel != "gasolina" && tipoCombustivel != "diesel");

            #endregion

            #region TELA DE SOLICITAÇÃO DO CONSUMO MÉDIO DO VEÍCULO

            float consumoMedio;
            bool isVarConsumoCombustivelNumeric;
            do
            {
                Console.Clear();
                Console.WriteLine("---------------------------------------------------------------------\n\n");
                Console.WriteLine(" Qual o consumo médio do seu veículo? (km/l)\n\n");
                Console.WriteLine("---------------------------------------------------------------------");
                isVarConsumoCombustivelNumeric = float.TryParse(Console.ReadLine(), out consumoMedio);
            } while (!isVarConsumoCombustivelNumeric);

            #endregion

            #region TELA DE SOLICITAÇÃO DO VALOR EM REAIS QUE PRETENDE ABASTECER

            float valorQueVaiAbastecer;
            bool isNumeric;
            do
            {
                Console.Clear();
                Console.WriteLine("---------------------------------------------------------------------\n\n");
                Console.WriteLine(" Quanto pretende abastecer agora? (R$)\n\n");
                Console.WriteLine("---------------------------------------------------------------------");
                isNumeric = float.TryParse(Console.ReadLine(), out valorQueVaiAbastecer);
            } while (!isNumeric);

            #endregion

            #region TELA LISTAGEM DOS POSTOS

            Console.Clear();
            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("                        Postos mais viáveis\n");
            Console.WriteLine("---------------------------------------------------------------------\n");

            Posto posto = new Posto();

            foreach (Posto item in posto.GetPostosViaveis(GetPostos(), valorQueVaiAbastecer, consumoMedio, tipoCombustivel))
            {
                Console.WriteLine($"NOME DO POSTO:\t\t\t{item.Nome}\n" +
                                  //$"TIPO DO COMBUSTIVEL:\t\t{item.GetCombustivelEmAnalise(tipoCombustivel).Tipo}\n" +
                                  //$"VALOR DO COMBUSTIVEL:\t\t{item.GetCombustivelEmAnalise(tipoCombustivel).Preco:C}\n" +
                                  $"VOLUME QUE SERÁ ABASTECIDO:\t{item.GetVolumeQueVaiAbastecer(valorQueVaiAbastecer, tipoCombustivel):n2} Litros\n" +
                                  $"COMBUSTIVEL GASTO ATÉ O POSTO:\t{item.GetCombustivelGastoAtePosto(consumoMedio):n2} Litros\n" +
                                  $"DELTACOMBUSTIVEL :\t\t{item.GetDeltaCombustivel(valorQueVaiAbastecer, tipoCombustivel, consumoMedio)}\n" +
                                  $"---------------------------------------------------------------------\n");
            }

            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("                        Postos mais baratos\n");
            Console.WriteLine("---------------------------------------------------------------------\n");
            foreach (Posto item in posto.GetPostosBaratos(GetPostos(), tipoCombustivel))
            {
                Console.WriteLine($"NOME DO POSTO:\t\t\t{item.Nome}\n" +
                                  $"TIPO DO COMBUSTIVEL:\t\t{tipoCombustivel}\n" +
                                  //$"PREÇO DO COMBUSTIVEL:\t\t{item.GetCombustivelEmAnalise(tipoCombustivel).Preco:C}\n" +
                                  $"VOLUME QUE SERÁ ABASTECIDO:\t{item.GetVolumeQueVaiAbastecer(valorQueVaiAbastecer, tipoCombustivel):n2} Litros\n" +
                                  $"COMBUSTIVEL GASTO ATÉ O POSTO:\t{item.GetCombustivelGastoAtePosto(consumoMedio):n2} Litros\n" +
                                  $"---------------------------------------------------------------------\n");
            }

            Console.WriteLine("---------------------------------------------------------------------\n");
            Console.WriteLine("                        Postos mais pertos\n");
            Console.WriteLine("---------------------------------------------------------------------\n");

            foreach (Posto item in posto.GetPostosPertos(GetPostos()))
            {
                Console.WriteLine($"NOME DO POSTO:\t\t\t{item.Nome}\n" +
                                  $"DISTÂNCIA ATÉ POSTO:\t\t{item.DistanciaAtePosto:n2} Km\n" +
                                  $"COMBUSTIVEL GASTO ATÉ O POSTO:\t{item.GetCombustivelGastoAtePosto(consumoMedio):n2} Litros\n" +
                                  $"---------------------------------------------------------------------\n");
            }

            #endregion

            Console.ReadKey();
        }
    }
}
