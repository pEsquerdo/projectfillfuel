﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    public class Usuario
    {
        #region Construtores

        public Usuario()
        {
        }

        #endregion

        #region Propriedades

        public string Nome { get; set; }
        public List<Veiculo> Veiculos { get; set; }
        public string LocalizacaoAtual { get; set; }//gambiarra
        //public Veiculo VeiculoEmUso { get; set; }

        #endregion

        #region Métodos

        public Veiculo GetVeiculoEmUso(string placa)
        {
            Veiculo veiculo = Veiculos.Find(c => c.Placa.Equals(placa.Replace(" ", "").ToUpper()));
            return veiculo;
        }

        #endregion
    }
}
