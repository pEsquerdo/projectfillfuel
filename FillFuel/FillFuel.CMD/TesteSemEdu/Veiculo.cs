﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.CMD
{
    public class Veiculo
    {
        #region Construtores

        public Veiculo()
        {
        }

        #endregion

        #region Propriedades

        public string TipoVeiculo { get; set; }
        public string Placa { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int AnoFabricacao { get; set; }
        public float VolumeTanque { get; set; }
        public string OpcaoCombustivel { get; set; }
        public float ConsumoMedio { get; set; }
        public float RelacaoDeParidadeEntreEtanolEGasolina { get; set; }//normalmente é 0.7, mas podemos fazer um método que o usuário testa isso na prática

        #endregion

        #region Métodos

        public void SetConsumoMedio()
        {
            //para usuário testar na prática
        }

        public void SetRelacaoDeParidadeEntreEtanolEGasolina()
        {
            //para usuário testar na prática
        }

        #endregion
    }
}
