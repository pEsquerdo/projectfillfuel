﻿using FillFuel.Mobile.DAO;
using FillFuel.Mobile.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillFuel.Mobile.Business
{
    public class AvaliacaoCombustivelBS
    {
        internal List<AvaliacaoCombustivelMD> Filtrar(List<AvaliacaoCombustivelMD> listaParaOrdenar, FiltroAbsolutoMD filtroAbsoluto)
        {
            switch (filtroAbsoluto.CombustivelSelecionado)
            {
                case TipoCombustivel.Etanol:
                    listaParaOrdenar = listaParaOrdenar.FindAll(c => c.NomeCombustivel == filtroAbsoluto.CombustivelSelecionado);
                    break;
                case TipoCombustivel.Gasolina:
                    listaParaOrdenar = listaParaOrdenar.FindAll(c => c.NomeCombustivel == filtroAbsoluto.CombustivelSelecionado);
                    break;
                case TipoCombustivel.Diesel:
                    listaParaOrdenar = listaParaOrdenar.FindAll(c => c.NomeCombustivel == filtroAbsoluto.CombustivelSelecionado);
                    break;
                case TipoCombustivel.Flex:
                    listaParaOrdenar = listaParaOrdenar.FindAll(c => c.NomeCombustivel == TipoCombustivel.Etanol || c.NomeCombustivel == TipoCombustivel.Gasolina);

                    foreach (var combust in listaParaOrdenar.FindAll(gasol => gasol.NomeCombustivel == TipoCombustivel.Gasolina))
                        combust.ValorCombustivel *= 0.7;

                    break;
                default:
                    break;
            }

            switch (filtroAbsoluto.FiltroSelecionado)
            {
                case TipoFiltro.Barato:
                    listaParaOrdenar = GetPostosBaratos(listaParaOrdenar);
                    break;
                case TipoFiltro.Perto:
                    listaParaOrdenar = GetPostosPertos(listaParaOrdenar);
                    break;
                case TipoFiltro.Viavel:
                    CalculaPontuacao(listaParaOrdenar, filtroAbsoluto);
                    listaParaOrdenar = GetPostosViaveis(listaParaOrdenar);
                    break;
                default:
                    break;
            }

            if (filtroAbsoluto.CombustivelSelecionado == TipoCombustivel.Flex)
                foreach (var combust in listaParaOrdenar.FindAll(gasol => gasol.NomeCombustivel == TipoCombustivel.Gasolina))
                    combust.ValorCombustivel /= 0.7;

            return listaParaOrdenar;
        }

        public List<AvaliacaoCombustivelMD> GetListaOriginal()
        {
            CombustivelDAO dao = new CombustivelDAO();

            return dao.ReadAll().ToList();
        }

        /// <summary>
        /// Retorna uma lista com postos mais pertos em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<AvaliacaoCombustivelMD> GetPostosPertos(List<AvaliacaoCombustivelMD> combustiveis)
        {
            return combustiveis.OrderBy(combustivel => combustivel.Distancia_Pontucacao).ToList();
        }

        /// <summary>
        /// Retorna uma lista com postos mais baratos em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<AvaliacaoCombustivelMD> GetPostosBaratos(List<AvaliacaoCombustivelMD> combustiveis)
        {
            return combustiveis.OrderBy(combustivel => combustivel.ValorCombustivel).ToList();
        }

        /// <summary>
        /// Retorna uma lista com postos mais viáveis em ordem crescente
        /// </summary>
        /// <returns></returns>
        public List<AvaliacaoCombustivelMD> GetPostosViaveis(List<AvaliacaoCombustivelMD> postos)
        {
            return postos.OrderByDescending(c => c.SaldoVolumeComb_Pontuacao).ToList();
        }

        /// <summary>
        /// Volume de combustível que pode ser abastecido com o valor que pretende gastar, descontado do volume de combustível necessário para ir até o posto
        /// </summary>
        /// <param name="valorQueVaiAbastecer"></param>
        /// <param name="tipoCombustivel"></param>
        /// <param name="consumoMedio"></param>
        /// <returns></returns>
        private double Calcula_SaldoVolumeComb_Pontuacao(AvaliacaoCombustivelMD combustivel, FiltroAbsolutoMD prop_Filtro)
        {
            return (prop_Filtro.ValorQueVaiAbastecer / combustivel.ValorCombustivel) - (combustivel.Distancia_Pontucacao / prop_Filtro.ConsumoMedio);
        }

        //private double Calcula_ValorTotal_Pontuacao(AvaliacaoCombustivelMD combustivel, double valorQueVaiAbastecer, double consumoMedio)
        //{
        //    return 0;
        //}

        public List<AvaliacaoCombustivelMD> CalculaPontuacao(List<AvaliacaoCombustivelMD> combustiveis, FiltroAbsolutoMD prop_Filtro)
        {
            List<AvaliacaoCombustivelMD> combustiveisAvaliados = new List<AvaliacaoCombustivelMD>();
            foreach (var combustivel in combustiveis)
            {
                combustivel.SaldoVolumeComb_Pontuacao = Calcula_SaldoVolumeComb_Pontuacao(combustivel, prop_Filtro);
                //combustivel.ValorTotal_Pontuacao = Calcula_ValorTotal_Pontuacao(combustivel);
            }
            return combustiveisAvaliados;
        }
    }
}
