﻿using FillFuel.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace FillFuel.Mobile.Business
{
    public class DistanciaBS
    {
        public DistanciaBS()
        {
           
        }

        public async Task<List<AvaliacaoCombustivelMD>> CalculaDistancia(List<AvaliacaoCombustivelMD> combustiveis)
        {            
            var locDisp = await Geolocation.GetLastKnownLocationAsync();
            foreach (var combustivel in combustiveis)
            {
                var locPosto = new Location(combustivel.Latitude, combustivel.Longitude);

                var distancia = Location.CalculateDistance(locDisp, locPosto, DistanceUnits.Kilometers);

                combustivel.Distancia_Pontucacao = distancia;
            }


            return combustiveis;
        }

    }
}
