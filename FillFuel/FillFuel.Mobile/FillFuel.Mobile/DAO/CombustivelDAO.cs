﻿using FillFuel.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FillFuel.Mobile.DAO
{
    public class CombustivelDAO
    {
        public void Inserir(AvaliacaoCombustivelMD combustivel)
        {
            var conn = Connection.GetConn();
            conn.Insert(combustivel);
            conn.Close();
        }
        public IEnumerable<AvaliacaoCombustivelMD> Read(Func<AvaliacaoCombustivelMD, bool> query)
        {
            var conn = Connection.GetConn();
            return conn.Table<AvaliacaoCombustivelMD>().Where(query).ToList();
        }
        public IEnumerable<AvaliacaoCombustivelMD> ReadAll()
        {
            var conn = Connection.GetConn();
            return conn.Table<AvaliacaoCombustivelMD>().ToList();
        }

        public void Update(AvaliacaoCombustivelMD newCombustivel)
        {
            var conn = Connection.GetConn();
            conn.Update(newCombustivel);
            conn.Close();
        }

        public void Delete(AvaliacaoCombustivelMD combustivel)
        {
            var conn = Connection.GetConn();
            conn.Delete(combustivel);
            conn.Close();
        }
    }
}
