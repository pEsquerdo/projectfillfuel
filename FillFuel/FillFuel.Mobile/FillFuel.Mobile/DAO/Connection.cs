﻿using FillFuel.Mobile.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FillFuel.Mobile.DAO
{
    public class Connection
    {
        public static SQLiteConnection GetConn()
        {
            var strConn = new StringConnection();
            var conn = new SQLiteConnection(strConn.GetString(), false);

            return conn;
        }
        public void StartDB()
        {
            var conn = Connection.GetConn();
            conn.BeginTransaction();
            conn.CreateTable<AvaliacaoCombustivelMD>();
            conn.Commit();
            conn.Close();

            Seeding();
        }

        public void Seeding()
        {
            var bancoInicial = new CombustivelDAO();
            var listaCombustiveis = bancoInicial.ReadAll();
            if (listaCombustiveis != null && listaCombustiveis.Count() > 0)
                return;

            var conn = Connection.GetConn();
            conn.BeginTransaction();


            DadosCombustivel();

            conn.Commit();
            conn.Close();
        }

        private void DadosCombustivel()
        {
            var combdao = new CombustivelDAO();


            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 1,
                Latitude = -20.814642,
                Longitude = -49.383043,
                NomeCombustivel = TipoCombustivel.Etanol,
                NomePosto = "Ipiranga Fênix",
                Endereco = "R. Voluntários de São Paulo, 3411 - Centro, São José do Rio Preto - SP, 15015-200",
                ValorCombustivel = 3.459
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 2,
                Latitude = -20.814642,
                Longitude = -49.383043,
                NomeCombustivel = TipoCombustivel.Gasolina,
                NomePosto = "Ipiranga Fênix",
                Endereco = "R. Voluntários de São Paulo, 3411 - Centro, São José do Rio Preto - SP, 15015-200",
                ValorCombustivel = 4.399
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 3,
                Latitude = -20.814642,
                Longitude = -49.383043,
                NomeCombustivel = TipoCombustivel.Diesel,
                NomePosto = "Ipiranga Fênix",
                Endereco = "R. Voluntários de São Paulo, 3411 - Centro, São José do Rio Preto - SP, 15015-200",
                ValorCombustivel = 2.699
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 4,
                Latitude = -20.810884,
                Longitude = -49.375712,
                NomeCombustivel = TipoCombustivel.Gasolina,
                NomePosto = "Cinquentão",
                Endereco = "Av. Duque de Caxias - Centro, São José do Rio Preto - SP, 15050-465",
                ValorCombustivel = 4.297
            });
            
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 5,
                Latitude = -20.810884,
                Longitude = -49.375712,
                NomeCombustivel = TipoCombustivel.Etanol,
                NomePosto = "Cinquentão",
                Endereco = "Av. Duque de Caxias - Centro, São José do Rio Preto - SP, 15050-465",
                ValorCombustivel = 2.899
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 6,
                Latitude = -20.810884,
                Longitude = -49.375712,
                NomeCombustivel = TipoCombustivel.Diesel,
                NomePosto = "Cinquentão",
                Endereco = "Av. Duque de Caxias - Centro, São José do Rio Preto - SP, 15050-465",
                ValorCombustivel = 3.699
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 7,
                Latitude = -20.802771,
                Longitude = -49.359071,
                NomeCombustivel = TipoCombustivel.Gasolina,
                NomePosto = "Chiesa Brazilian",
                Endereco = "Av. Dr. Fernando Costa, 1549 - Jardim Primavera, São José do Rio Preto - SP, 15061-000",
                ValorCombustivel = 3.899
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 8,
                Latitude = -20.802771,
                Longitude = -49.359071,
                NomeCombustivel = TipoCombustivel.Etanol,
                NomePosto = "Chiesa Brazilian",
                Endereco = "Av. Dr. Fernando Costa, 1549 - Jardim Primavera, São José do Rio Preto - SP, 15061-000",
                ValorCombustivel = 2.699
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 9,
                Latitude = -20.802771,
                Longitude = -49.359071,
                NomeCombustivel = TipoCombustivel.Diesel,
                NomePosto = "Chiesa Brazilian",
                Endereco = "Av. Dr. Fernando Costa, 1549 - Jardim Primavera, São José do Rio Preto - SP, 15061-000",
                ValorCombustivel = 3.599
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 10,
                Latitude = -20.825081,
                Longitude = -49.366503,
                NomeCombustivel = TipoCombustivel.Gasolina,
                NomePosto = "Hipocampos",
                Endereco = "Av. Murchid Homsi, 1602 - Mansur Daud, São José do Rio Preto - SP, 15042-102",
                ValorCombustivel = 4.300
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 11,
                Latitude = -20.825081,
                Longitude = -49.366503,
                NomeCombustivel = TipoCombustivel.Etanol,
                NomePosto = "Hipocampos",
                Endereco = "Av. Murchid Homsi, 1602 - Mansur Daud, São José do Rio Preto - SP, 15042-102",
                ValorCombustivel = 3.099
            });
            
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 12,
                Latitude = -20.825081,
                Longitude = -49.366503,
                NomeCombustivel = TipoCombustivel.Diesel,
                NomePosto = "Hipocampos",
                Endereco = "Av. Murchid Homsi, 1602 - Mansur Daud, São José do Rio Preto - SP, 15042-102",
                ValorCombustivel = 4.099
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 13,
                Latitude = -20.821090,
                Longitude = -49.399404,
                NomeCombustivel = TipoCombustivel.Gasolina,
                NomePosto = "Tropical I",
                Endereco = "Av. Bady Bassitt, 5170 - Jardim Alto Rio Preto, São José do Rio Preto - SP, 15025-000",
                ValorCombustivel = 4.590
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 14,
                Latitude = -20.821090,
                Longitude = -49.399404,
                NomeCombustivel = TipoCombustivel.Diesel,
                NomePosto = "Tropical I",
                Endereco = "Av. Bady Bassitt, 5170 - Jardim Alto Rio Preto, São José do Rio Preto - SP, 15025-000",
                ValorCombustivel = 3.299
            });
            combdao.Inserir(new AvaliacaoCombustivelMD()
            {
                Id = 15,
                Latitude = -20.821090,
                Longitude = -49.399404,
                NomeCombustivel =TipoCombustivel.Etanol,
                NomePosto = "Tropical I",
                Endereco = "Av. Bady Bassitt, 5170 - Jardim Alto Rio Preto, São José do Rio Preto - SP, 15025-000",
                ValorCombustivel =2.890
            });

        }
    }
}
