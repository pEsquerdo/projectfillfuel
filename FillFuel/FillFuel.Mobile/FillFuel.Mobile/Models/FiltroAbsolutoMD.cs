﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FillFuel.Mobile.Models
{
    public class FiltroAbsolutoMD
    {
        public TipoCombustivel CombustivelSelecionado { get; set; }
        public TipoFiltro FiltroSelecionado { get; set; }
        public double ValorQueVaiAbastecer { get; set; }
        public double ConsumoMedio { get; set; }
    }
}
