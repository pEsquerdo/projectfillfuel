﻿using FillFuel.Mobile.Business;
using FillFuel.Mobile.DAO;
using FillFuel.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace FillFuel.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CatalogoPostoView : ContentPage
    {
        DistanciaBS distBs = new DistanciaBS();

        public static FiltroAbsolutoMD FILTRO_ABSOLUTO = new FiltroAbsolutoMD();
        public AvaliacaoCombustivelBS bs = new AvaliacaoCombustivelBS();
        public List<AvaliacaoCombustivelMD> listaCalculadaOriginal = new List<AvaliacaoCombustivelMD>();

        public CatalogoPostoView()
        {
            InitializeComponent();
            FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Flex;
            FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Viavel;
            FILTRO_ABSOLUTO.ConsumoMedio = 11.55;
            FILTRO_ABSOLUTO.ValorQueVaiAbastecer = 100;
        }

        private void BtnFiltrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new FiltroView());
        }

        private void VCatalogoPosto_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var posto = e.SelectedItem as AvaliacaoCombustivelMD;

            Navigation.PushAsync(new PostoView(listaCalculadaOriginal.FindAll(c => c.NomePosto == posto.NomePosto)));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            pickerCombustivel.SelectedIndex = FILTRO_ABSOLUTO.CombustivelSelecionado.GetHashCode();
            pickerOrdenarPor.SelectedIndex = FILTRO_ABSOLUTO.FiltroSelecionado.GetHashCode();

            //sv =SERVIÇO XAMARIM ESSENTIAL PARA CALCULAR A DISTANCIA
            var origi = bs.GetListaOriginal();
            listaCalculadaOriginal = await distBs.CalculaDistancia(origi);
            vCatalogoPosto.ItemsSource = bs.Filtrar(listaCalculadaOriginal.ToList(), FILTRO_ABSOLUTO);
        }

        private void BtnAdicionar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PostoView());
        }

        private void pickerCombustivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            string pickerStr = picker.SelectedItem.ToString();

            switch (pickerStr)
            {
                case "Diesel":
                    FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Diesel;
                    break;
                case "Etanol":
                    FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Etanol;
                    break;
                case "Gasolina":
                    FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Gasolina;
                    break;
                case "Flex":
                    FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Flex;
                    break;
                case "Todos":
                    FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Todos;
                    break;
                default:
                    break;
            }

            vCatalogoPosto.ItemsSource = bs.Filtrar(listaCalculadaOriginal.ToList(), FILTRO_ABSOLUTO);
        }

        private void pickerOrdenarPor_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            string pickerStr = picker.SelectedItem.ToString();

            switch (pickerStr)
            {
                case "Maior economia":
                    FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Viavel;
                    break;
                case "Menor preço":
                    FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Barato;
                    break;
                case "Menor distância":
                    FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Perto;
                    break;
                default:
                    break;
            }

            vCatalogoPosto.ItemsSource = bs.Filtrar(listaCalculadaOriginal.ToList(), FILTRO_ABSOLUTO);
        }
    }
}
