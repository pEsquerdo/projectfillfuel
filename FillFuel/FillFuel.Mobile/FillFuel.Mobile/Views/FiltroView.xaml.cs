﻿using FillFuel.Mobile.Business;
using FillFuel.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FillFuel.Mobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FiltroView : ContentPage
	{
		public FiltroView ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            textBoxConsumoMedio.Text = CatalogoPostoView.FILTRO_ABSOLUTO.ConsumoMedio.ToString("F2");
            textBoxValorGasto.Text = CatalogoPostoView.FILTRO_ABSOLUTO.ValorQueVaiAbastecer.ToString("F2");
            pickerComustivel.SelectedIndex = CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado.GetHashCode();
            pickerTipoLista.SelectedIndex = CatalogoPostoView.FILTRO_ABSOLUTO.FiltroSelecionado.GetHashCode();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            double.TryParse(textBoxValorGasto.Text, out double valorGasto);
            CatalogoPostoView.FILTRO_ABSOLUTO.ValorQueVaiAbastecer = valorGasto;

            double.TryParse(textBoxConsumoMedio.Text, out double consumoMedio);
            CatalogoPostoView.FILTRO_ABSOLUTO.ConsumoMedio = consumoMedio;
        }

        private void PickerComustivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            string pickerStr = picker.SelectedItem.ToString();

            switch (pickerStr)
            {
                case "Diesel":
                    CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Diesel;
                    break;
                case "Etanol":
                    CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Etanol;
                    break;
                case "Gasolina":
                    CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Gasolina;
                    break;
                case "Flex":
                    CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Flex;
                    break;
                case "Todos":
                    CatalogoPostoView.FILTRO_ABSOLUTO.CombustivelSelecionado = TipoCombustivel.Todos;
                    break;
                default:
                    break;
            }
        }
        
        private void PickerTipoLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            string pickerStr = picker.SelectedItem.ToString();

            switch (pickerStr)
            {
                case "Viável":
                    CatalogoPostoView.FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Viavel;
                    break;
                case "Barato":
                    CatalogoPostoView.FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Barato;
                    break;
                case "Próximo":
                    CatalogoPostoView.FILTRO_ABSOLUTO.FiltroSelecionado = TipoFiltro.Perto;
                    break;
                default:
                    break;
            }
        }

        private void BtnFiltrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
