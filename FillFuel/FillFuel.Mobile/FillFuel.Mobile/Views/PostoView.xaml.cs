﻿using FillFuel.Mobile.DAO;
using FillFuel.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FillFuel.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostoView : ContentPage
    {
        private CombustivelDAO dao = new CombustivelDAO();        
        
        AvaliacaoCombustivelMD etanol;
        AvaliacaoCombustivelMD gasolina;
        AvaliacaoCombustivelMD diesel;

        public PostoView()
        {
            InitializeComponent();

            this.lblLatitude.IsVisible = true;
            this.lblLongitude.IsVisible = true;
            this.vLatitude.IsVisible = true;
            this.vLongitude.IsVisible = true;
            this.btnDelete.IsVisible = false;

            etanol = new AvaliacaoCombustivelMD();
            gasolina = new AvaliacaoCombustivelMD();
            diesel = new AvaliacaoCombustivelMD();
        }

        public PostoView(List<AvaliacaoCombustivelMD> combustiveis)
        {
            InitializeComponent();

            this.lblLatitude.IsVisible = false;
            this.lblLongitude.IsVisible = false;
            this.vLatitude.IsVisible = false;
            this.vLongitude.IsVisible = false;
            this.btnDelete.IsVisible = true;
           
            this.vNome.Text = combustiveis.FirstOrDefault().NomePosto;

            foreach (var combustivel in combustiveis)
            {
                if (combustivel.NomeCombustivel == TipoCombustivel.Etanol)
                    etanol = combustivel;
                else if (combustivel.NomeCombustivel == TipoCombustivel.Gasolina)
                    gasolina = combustivel;
                else if (combustivel.NomeCombustivel == TipoCombustivel.Diesel)
                    diesel = combustivel;
            }
            this.vPrecoEtanol.Text = etanol.ValorCombustivel.ToString();
            this.vPrecoGasolina.Text = gasolina.ValorCombustivel.ToString();
            this.vPrecoDiesel.Text = diesel.ValorCombustivel.ToString();
            this.vEndereco.Text = etanol.Endereco;
        }

       
        private void BtnSalvar_Clicked(object sender, EventArgs e)
        {
            var comb = new AvaliacaoCombustivelMD();
            comb.Id = gasolina.Id;

            gasolina.NomePosto = diesel.NomePosto = etanol.NomePosto = this.vNome.Text;
            gasolina.Endereco = diesel.Endereco = etanol.Endereco = this.vEndereco.Text;

            gasolina.ValorCombustivel = double.Parse(this.vPrecoGasolina.Text);
            gasolina.NomeCombustivel = TipoCombustivel.Gasolina;

            diesel.ValorCombustivel = double.Parse(this.vPrecoDiesel.Text);
            diesel.NomeCombustivel = TipoCombustivel.Diesel;

            etanol.ValorCombustivel = double.Parse(this.vPrecoEtanol.Text);
            etanol.NomeCombustivel = TipoCombustivel.Etanol;


            if (comb.Id == 0)
            {
                dao.Inserir(gasolina);
                dao.Inserir(etanol);
                dao.Inserir(diesel);
            }
            else
            {
                dao.Update(etanol);
                dao.Update(gasolina);
                dao.Update(diesel);
            }

            Navigation.PopAsync();         
        }

        private async void BtnDelete_Clicked(object sender, EventArgs e)
        {

            bool result = await DisplayAlert("Deletar", $"Deseja realmente apagar o { etanol.NomePosto}?", "Sim, Apagar", "Não");

            if(result)
            {
                dao.Delete(etanol);
                dao.Delete(gasolina);
                dao.Delete(diesel);
                await DisplayAlert("Alerta", "Posto apagado com sucesso", "Fechar");
                await Navigation.PopAsync();
            }       
        }
    }
}
